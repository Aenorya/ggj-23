using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Introduction : MonoBehaviour
{

    [SerializeField] private Button nextBtn;
    public float delayBtn = 1f;
    [SerializeField] private GameObject firstText, secondText, tutorial;

    private int step = 0;
    // Start is called before the first frame update
    void Start()
    {
        nextBtn.onClick.AddListener(delegate
        {
            step++;
            switch (step)
            {
                case 1:
                    firstText.SetActive(false);
                    secondText.SetActive(true);
                    break;
                case 2:
                    secondText.SetActive(false);
                    tutorial.SetActive(true);
                    break;
                case 3:
                    SceneManager.LoadScene(2);
                    break;
            }
            nextBtn.gameObject.SetActive(false);
            Invoke("ShowBtn", delayBtn);
        });
        nextBtn.gameObject.SetActive(false);
        Invoke("ShowBtn", delayBtn);
    }

    void ShowBtn()
    {
        nextBtn.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
