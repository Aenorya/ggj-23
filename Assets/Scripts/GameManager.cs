using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public Planter planter;

    private void Awake()
    {
        if (Instance) Destroy(this);
        else Instance = this;
    }

    
}
