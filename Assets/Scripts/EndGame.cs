using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI endText;
    [SerializeField] private Button quit, stay;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        stay.onClick.AddListener(delegate
        {
            gameObject.SetActive(false);
            Time.timeScale = 1;
        });
        quit.onClick.AddListener(delegate
        {
            endText.text = "Thank you for playing!";
            stay.gameObject.SetActive(false);
            quit.gameObject.SetActive(false);
            Invoke("Quit", 2f);
        });
    }

    void Quit()
    {
        Application.Quit();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
