using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Balance")]
public class GameBalance : ScriptableObject
{
    [Header("Requests rewards")] public Vector2Int easyQuestBracket;
    public Vector2Int mediumQuestBracket, hardQuestBracket;
    
    [Header("Recharge quantity / second nutrients in soil")]
    public float nitrogen;
    public float phosphorus, potassium, minerals;

    [Header("Starter Inventory")] 
    public int money;

    public List<InventorySeed> starterSeeds;
}
[Serializable]
public class InventorySeed
{
    public SeedData seedData;
    public int quantity;

    public InventorySeed(SeedData s, int qtt)
    {
        seedData = s;
        quantity = qtt;
    }
}
