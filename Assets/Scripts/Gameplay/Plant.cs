using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Plant : MonoBehaviour
{
    [SerializeField] private SeedData seed;
    [SerializeField] private Soil growingSoil;
    [SerializeField] private float remainingTime = 10000;
    private TextMeshProUGUI completionTxt;
    private Image completionFill;
    float n, p, k, m;

    public void Setup(Soil callback, SeedData origin, TextMeshProUGUI completion, Image filler)
    {
        growingSoil = callback;
        seed = origin;
        n  = seed.nitrogen / seed.growingTime;
        p  = seed.phosphorus / seed.growingTime;
        k  = seed.potassium / seed.growingTime;
        m  = seed.minerals / seed.growingTime;
        remainingTime = seed.growingTime;
        completionTxt = completion;
        completionFill = filler;
    }

    public void Update()
    {
        if (remainingTime > 0 
            && growingSoil.UseNutrients(n*Time.deltaTime, p*Time.deltaTime, k*Time.deltaTime, m*Time.deltaTime))
        {
            remainingTime -= Time.deltaTime;
        }else if (remainingTime <= 0)
        {
            growingSoil.OnPlantReady();
        }
        
        if (completionTxt)
        {
            float progress = (seed.growingTime - remainingTime) / seed.growingTime;
            completionTxt.text = string.Format("{0:0} %", (progress * 100));
            completionFill.fillAmount = progress;
        }
    }

    public PlantTags GetTags()
    {
        return seed.tags;
    }
}
