using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Soil : MonoBehaviour
{
    [Header("Current quantity of nutrients in the soil")] 
    [SerializeField]
    private float nitrogen = 20f;
    [SerializeField] 
    private float phosphorus = 20, potassium = 20, minerals = 15;
    private float maxNitrogen = 20, maxPhosphorus = 20, maxPotassium = 20, maxMinerals = 15;
    [SerializeField] 
    private bool watered = false;
    
    [SerializeField] private int waterCounter = 0;
    [SerializeField] private GameBalance _balance;
    [SerializeField] private Plant _plant;

    [SerializeField] private TextMeshProUGUI nText, pText, kText, minText, completionText;
    [SerializeField] private GameObject completionTxtContainer;
    [SerializeField] private Image completionFill;
    [SerializeField] private Button pickupBtn;

    [SerializeField] private Request currentRequest;

    private bool isPlantReady = false;

    private void Start()
    {
        pickupBtn.onClick.AddListener(PickPlant);
        completionText.text = "-- %";
        completionFill.fillAmount = 0;
    }

    void Update()
    {
        nitrogen = Mathf.Min(nitrogen + _balance.nitrogen * Time.deltaTime, maxNitrogen);
        nText.text = string.Format("{0:F1} / {1:00}", nitrogen, maxNitrogen);
        phosphorus = Mathf.Min(phosphorus + _balance.nitrogen * Time.deltaTime, maxPhosphorus);
        pText.text = string.Format("{0:F1} / {1:00}", phosphorus, maxPhosphorus);
        potassium = Mathf.Min(potassium + _balance.nitrogen * Time.deltaTime, maxPotassium);
        kText.text = string.Format("{0:F1} / {1:00}", potassium, maxPotassium);
        minerals = Mathf.Min(minerals + _balance.nitrogen * Time.deltaTime, maxMinerals);
        minText.text = string.Format("{0:F1} / {1:00}", minerals, maxMinerals);
    }

    public Plant GetPlant()
    {
        return _plant;
    }

    public bool UseNutrients(float n, float p, float k, float m)
    {
        if (nitrogen < 0.1 || phosphorus < 0.1 || potassium < 0.1 || minerals < 0.1
            || nitrogen < n || phosphorus < p || potassium < k || minerals < m) return false;
        nitrogen -= n;
        phosphorus -= p;
        potassium -= k;
        minerals -= m;
        return true;
    }


    void PickPlant()
    {
        // Ask for the quest to complete !
        if (!_plant) return;
        watered = false;
        waterCounter = 0;
        Destroy(_plant.gameObject);
        _plant = null;
        isPlantReady = false;
        completionText.text = "-- %";
        completionFill.fillAmount = 0;
        completionTxtContainer.SetActive(true);
        pickupBtn.gameObject.SetActive(false);
        GetComponent<SphereCollider>().enabled = true;
        
        RequestManager.Instance.CompleteRequest(currentRequest);
    }
    

    public void OnPlantReady()
    {
        //GetComponent<SphereCollider>().enabled = true;
        isPlantReady = true;
        //completionTxtContainer.gameObject.SetActive(false);
        RequestManager.Instance.Notify(this);
    }

    public void ActivatePickup(Request request)
    {
        currentRequest = request;
        completionTxtContainer.gameObject.SetActive(false);
        pickupBtn.gameObject.SetActive(true);
    }

    public void DisablePickup()
    {
        completionTxtContainer.gameObject.SetActive(true);
        pickupBtn.gameObject.SetActive(false);
    }

    public bool IsPlantReady()
    {
        return _plant && isPlantReady;
    }

    public void PlantSeed(SeedData seed)
    {
        GameObject plantGO = Instantiate(seed.plantPrefab, transform);
        _plant = plantGO.GetComponent<Plant>();
        _plant.Setup(this, seed, completionText, completionFill);
        GetComponent<SphereCollider>().enabled = false;
    }

    public bool IsSoilCompatible(SeedData seed)
    {
        return !_plant
               && nitrogen >= seed.nitrogen
               && potassium >= seed.potassium
               && phosphorus >= seed.phosphorus
               && minerals >= seed.minerals;
    }
}
