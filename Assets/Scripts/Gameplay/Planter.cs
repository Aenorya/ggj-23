using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planter : MonoBehaviour
{
    
    public Color soilHover;
    public SeedData chosenSeed;
    public Soil hovered;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            enabled = false;
            if(hovered)hovered.GetComponent<MeshRenderer>().material.color = Color.white;
                
        }
        if (UIManager.Instance.openedUi == null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool raycast = Physics.Raycast(ray, out hit);
            if (hovered && (!raycast || hit.transform != hovered.transform))
            {
                hovered.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
                hovered = null;
            }
            else if (raycast && !hovered)
            {
                if (hit.transform.CompareTag("Soil"))
                {
                    hovered = hit.transform.GetComponentInChildren<Soil>();
                    hovered.GetComponentInChildren<MeshRenderer>().material.color = soilHover;
                }
                
            }
            else if (raycast && hovered && Input.GetMouseButtonDown(0))
            {
                hovered.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
                print(hit.transform.name);
                hovered.PlantSeed(chosenSeed);
                Inventory.Instance.RemoveSeed(chosenSeed);
                enabled = false;
            }
        }
    }
}
