using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static Inventory Instance;
    public static int Money = 0;
    private static List<InventorySeed> _seeds = new List<InventorySeed>();

    [SerializeField] private GameBalance balance;
    [SerializeField] private TextMeshProUGUI moneyText;

    private void Awake()
    {
        if (Instance) Destroy(this);
        else Instance = this;

        Money = balance.money;
        foreach (InventorySeed seed in balance.starterSeeds)
        {
            _seeds.Add(new InventorySeed(seed.seedData, seed.quantity));
            print($"Added {seed.quantity} {seed.seedData.label}");
        }
        UpdateMoneyUI();
    }

    public static List<InventorySeed> GetSeeds()
    {
        return _seeds;
    }

    public static void BuySeed(SeedData seed, int quantity, int cost)
    {
        int index = _seeds.FindIndex(s => s.seedData == seed);
        if (index == -1)
        {
            _seeds.Add(new InventorySeed(seed, quantity));
        }
        else
        {
            _seeds[index].quantity += quantity;
        }
        Money -= cost;
        Instance.UpdateMoneyUI();
    }

    public void RemoveSeed(SeedData seed)
    {
        int index = _seeds.FindIndex(s => s.seedData == seed);
        if (index != -1)
        {
            _seeds[index].quantity--;
        }
    }
    public static void Earn(int earnings)
    {
        Money += earnings;
        Instance.UpdateMoneyUI();
    }

    private void UpdateMoneyUI()
    {
        moneyText.text = Money.ToString();
    }
}
