using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RequestManager : MonoBehaviour
{
    public static RequestManager Instance;
    
    public Dictionary<Request, GameObject> requestPanels = new Dictionary<Request, GameObject>();
    public GameObject requestPrefab, requestContainer;
    private List<Soil> _soils = new List<Soil>();
    private void Awake()
    {
        if(Instance) Destroy(this);
        else Instance = this;

        _soils = FindObjectsOfType<Soil>().ToList();
    }

    public void AddRequest(Request request)
    {
        GameObject requestPanel = Instantiate(requestPrefab, requestContainer.transform);
        requestPanels.Add(request, requestPanel);
        requestPanels[request].transform.GetChild(1)
            .GetComponent<TextMeshProUGUI>().text = SeedbagElement.TagToStr(request.tags);
        requestPanels[request].transform.GetChild(0)
            .GetComponent<TextMeshProUGUI>().text = $"{request.time} day(s)";
    }

    public void Notify(Soil soil)
    {
        
        foreach (Request request in requestPanels.Keys)
        {
            if (soil.GetPlant() && (request.tags & soil.GetPlant().GetTags())  == request.tags)
            {
                requestPanels[request].GetComponent<Button>().interactable = true;
                Request requestCopy = request;
                requestPanels[request].GetComponent<Button>().onClick.AddListener(delegate
                {
                    foreach (Soil s in _soils)
                    {
                        if(s.IsPlantReady() && (requestCopy.tags & s.GetPlant().GetTags())  == requestCopy.tags)
                        {
                            s.ActivatePickup(requestCopy);
                        }
                    }
                });
            }
        }
    }

    public void OnDayPassed()
    {
        List<Request> toRemove = new List<Request>();
        foreach (Request request in requestPanels.Keys)
        {
            request.DayPassed();
            if (request.time < 1)
            {
                toRemove.Add(request);
                
            }
            else
            {
                requestPanels[request].transform.GetChild(0)
                    .GetComponent<TextMeshProUGUI>().text = $"{request.time} day(s)";
                if (request.time == 1)
                {
                    requestPanels[request].transform.GetChild(0)
                        .GetComponent<TextMeshProUGUI>().color = Color.red;
                }
            }
        }

        while (toRemove.Count > 0)
        {
            if(requestPanels.ContainsKey(toRemove[0])){
                Destroy(requestPanels[toRemove[0]]);
                requestPanels.Remove(toRemove[0]);
            }
            toRemove.RemoveAt(0);
        }
    }

    public void CompleteRequest(Request request)
    {
        if(!requestPanels.ContainsKey(request)) return;
        GameObject panel = requestPanels[request];
        requestPanels.Remove(request);
        Destroy(panel);
        
        Inventory.Earn(request.reward);
        foreach (Soil soil in _soils)
        {
            soil.DisablePickup();
        }
    }

    public bool CanAddRequest()
    {
        return requestPanels.Count < 6;
    }
}

public class Request
{
    public PlantTags tags;
    public int reward;
    public int time;

    public Request(PlantTags t, int r, int ti)
    {
        tags = t;
        reward = r;
        time = ti;
    }

    public void DayPassed()
    {
        time--;
    }
}