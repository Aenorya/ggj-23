using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Seed Data")]
public class SeedData : ScriptableObject
{
    public string label;
    public GameObject plantPrefab;
    public PlantTags tags;
    public int growingTime;
    public float nitrogen, potassium, phosphorus, minerals;

    
}
