
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    public static int day = 1;
    private float time = 0;

    [SerializeField] private TextMeshProUGUI dayTxt, timeTxt;
    [SerializeField] private Button pauseBtn, playBtn, fastBtn;
    void Start()
    {
        pauseBtn.onClick.AddListener(delegate
        {
            Time.timeScale = 0;
        });
        
        playBtn.onClick.AddListener(delegate
        {
            Time.timeScale = 1;
        });
        
        fastBtn.onClick.AddListener(delegate
        {
            Time.timeScale = 4;
        });
    }

    void Update()
    {
        time += ((60/5)*Time.deltaTime);
        timeTxt.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(time / 60.0f), (time % 60));
        if (time / 60.0f >= 24)
        {
            time = 0;
            day++;
            dayTxt.text = $"Day {day}";
            RequestManager.Instance.OnDayPassed();
        }
    }
}
