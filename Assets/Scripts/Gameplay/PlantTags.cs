
using System;

[Flags]
public enum PlantTags
{
   Red=1<<0,
   Pink=1<<1,
   Yellow=1<<2,
   Green=1<<3,
   Blue=1<<4,
   Purple=1<<5,
   White=1<<6,
   Black=1<<7,
   Flower=1<<8,
   Leafy= 1<<9,
   Big= 1<<10,
   Small = 1<<11,

}
