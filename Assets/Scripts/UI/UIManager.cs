using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    public GameObject openedUi;

    [SerializeField] private GameObject requestCanvas, shopCanvas, inventoryCanvas;

    private void Awake()
    {
        if (Instance) Destroy(this);
        else Instance = this;
    }

    public void OpenRequest()
    {
        OpenUI(Canvases.Request);
    }

    public void OpenShop()
    {
        OpenUI(Canvases.Shop);
    }

    public void OpenInventory()
    {
        OpenUI(Canvases.Inventory);
    }

    private void OpenUI(Canvases canvas)
    {
        Time.timeScale = 0;
        if (openedUi) return;
        switch (canvas)
        {
            case Canvases.Request:
                requestCanvas.SetActive(true);
                openedUi = requestCanvas;
                break;
            case Canvases.Shop:
                shopCanvas.SetActive(true);
                openedUi = shopCanvas;
                break;
            case Canvases.Inventory:
                inventoryCanvas.SetActive(true);
                openedUi = inventoryCanvas;
                break;
        }
    }

    public void CloseUI()
    {
        openedUi.SetActive(false);
        openedUi = null;
        Time.timeScale = 1.0f;
    }
}

public enum Canvases
{
    Request,
    Shop,
    Inventory
}