using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RequestPanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI request, reward, time;
    [SerializeField] private Button accept;
    private Request quest;
    private void Start()
    {
        accept.onClick.AddListener(AcceptRequest);
    }

    public void Init(PlantTags tags, int money, int days)
    {
        quest = new Request(tags, money, days);
        request.text = SeedbagElement.TagToStr(tags);
        reward.text = money.ToString();
        time.text = $"{days} days";
        accept.interactable = true;
    }

    public void AcceptRequest()
    {
        accept.interactable = false;
        RequestManager.Instance.AddRequest(quest);
    }
}
