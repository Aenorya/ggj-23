
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RequestsUI : MonoBehaviour
{
    public List<RequestPanel> requests = new List<RequestPanel>();
    [SerializeField] private GameBalance _balance;
    private int lastUpdate = 0;
    

    private void Start()
    {
        GenerateNewRequests();
    }

    private void OnEnable()
    {
        if (lastUpdate != TimeManager.day)
        {
            GenerateNewRequests();
            lastUpdate = TimeManager.day;
        }
        
    }

    private void GenerateNewRequests()
    {
        //Easy quest
        int rand = Random.Range(0, 12);
        PlantTags tags = (PlantTags) (1 << rand);
        int money = Random.Range(_balance.easyQuestBracket.x, _balance.easyQuestBracket.y);
        int time = Random.Range(1, 3);
        requests[0].Init(tags, money, time);
        //Easy quest
        rand = Random.Range(0, 12);
        tags = (PlantTags) (1 << rand);
        money = Random.Range(_balance.easyQuestBracket.x, _balance.easyQuestBracket.y);
        time = Random.Range(1, 3);
        requests[1].Init(tags, money, time);
        
        //Medium quest
        rand = Random.Range(0, 12);
        tags = (PlantTags) (1 << rand);
        int rand2 = rand;
        do
        {
            rand2 = Random.Range(0, 12);
        } while (rand == rand2);

        Debug.Log(tags);
        Debug.Log(rand2);
        Debug.Log((PlantTags)rand2);
        tags |= (PlantTags) (1 << rand2);
        Debug.Log(tags);

        money = Random.Range(_balance.mediumQuestBracket.x, _balance.mediumQuestBracket.y);
        time = Random.Range(2, 4);
        requests[2].Init(tags, money, time);
        
        //Hard quest
        rand = Random.Range(0, 12);
        tags = (PlantTags) (1 << rand);
        do
        {
            rand2 = Random.Range(0, 12);
        } while (rand == rand2);

        int rand3 = rand;
        do
        {
            rand3 = Random.Range(0, 12);
        } while (rand3 == rand2 || rand3 == rand);

        tags |= (PlantTags) (1 << rand2) | (PlantTags) (1 << rand3);
        
        money = Random.Range(_balance.hardQuestBracket.x, _balance.hardQuestBracket.y);
        time = Random.Range(3, 6);
        requests[3].Init(tags, money, time);
    }

}


