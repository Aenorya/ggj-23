using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    [SerializeField] private GameObject seedOptionPrefab, scrollviewContent;
    private Dictionary<GameObject, InventorySeed> seedPanels = new Dictionary<GameObject, InventorySeed>();
    private void OnEnable()
    {
        List<GameObject> toRemove = new List<GameObject>();
        foreach (var panel in seedPanels)
        {
            if (panel.Value.quantity <= 0)
            {
                Debug.Log("Remove "+ panel.Value.seedData.label);
                toRemove.Add(panel.Key);
                Inventory.Instance.RemoveSeed(panel.Value.seedData);
            }
            else panel.Key.GetComponent<SeedbagElement>().UpdatePanel(panel.Value.quantity);

        }

        while (toRemove.Count > 0)
        {
            seedPanels.Remove(toRemove[0]);
            Destroy(toRemove[0]);
            toRemove.RemoveAt(0);
        }

        foreach (var seed in Inventory.GetSeeds())
        {
            if (!seedPanels.ContainsValue(seed) && seed.quantity > 0)
            {
                Debug.Log("Add "+ seed.seedData.label);
                GameObject panel = Instantiate(seedOptionPrefab, scrollviewContent.transform);
                panel.GetComponent<SeedbagElement>().SetupSeed(this, seed);
                seedPanels.Add(panel, seed);
                
            }
        }
    }

    
}
