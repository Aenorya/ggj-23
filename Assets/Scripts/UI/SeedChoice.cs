
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SeedChoice : MonoBehaviour
{
    [SerializeField] private SeedData seed;

    [SerializeField] private TextMeshProUGUI label, tagsTxt, nQtt, pQtt, kQtt, mQtt, costTxt, timeTxt;

    [SerializeField] private Button buyBtn;

    private int quantity = 1, cost = 0;
    private SeedData seedToBuy;
    private bool available = true;

    private void Start()
    {
        buyBtn.onClick.AddListener(Buy);
    }

    private void OnEnable()
    {
        
    }

    public void Init(SeedData seed)
    {
        seedToBuy = seed;
        quantity = Random.Range(1, 5);
        cost = Random.Range(10, 31) * quantity;
        label.text = $"{quantity} {seed.label}";
        tagsTxt.text = SeedbagElement.TagToStr(seed.tags);
        nQtt.text = seed.nitrogen.ToString();
        pQtt.text = seed.phosphorus.ToString();
        kQtt.text = seed.potassium.ToString();
        mQtt.text = seed.minerals.ToString();
        costTxt.text = cost.ToString();
        timeTxt.text = $"{seed.growingTime / 5} hours";
        available = true;
    }

    private void Update()
    {
        if(available) buyBtn.interactable = (cost <= Inventory.Money);
    }


    void Buy()
    {
        Inventory.BuySeed(seedToBuy, quantity, cost);
        buyBtn.interactable = false;
        available = false;
    }
    
}
