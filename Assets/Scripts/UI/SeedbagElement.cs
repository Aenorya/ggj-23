using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SeedbagElement : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI label, tags, nQTT, pQTT, kQtt, mQtt, time, totalSeeds;
    [SerializeField] private Button useBtn;

    private SeedData seed;
    private InventoryUI ui;

    public void SetupSeed(InventoryUI callback, InventorySeed invSeed)
    {
        ui = callback;
        seed = invSeed.seedData;
        label.text = seed.label;
        tags.text = TagToStr(seed.tags);
        nQTT.text = seed.nitrogen.ToString();
        pQTT.text = seed.phosphorus.ToString();
        kQtt.text = seed.potassium.ToString();
        mQtt.text = seed.minerals.ToString();

        time.text = $"{seed.growingTime / 5} hours";
        totalSeeds.text = invSeed.quantity.ToString();
        useBtn.onClick.AddListener(delegate
        {
            GameManager.Instance.planter.enabled = true;
            GameManager.Instance.planter.chosenSeed = seed;
            UIManager.Instance.CloseUI();
        });
    }

    public void UpdatePanel(int quantity)
    {
        totalSeeds.text = quantity.ToString();
    }

    public static string TagToStr(PlantTags plantTags)
    {
        string tagStr = "";
        bool first = true;

        if ((plantTags & PlantTags.Red) != 0)
        {
            tagStr += "Red";
            first = false;
        }

        if ((plantTags & PlantTags.Pink) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Pink";
            first = false;
        }

        if ((plantTags & PlantTags.Yellow) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Yellow";
            first = false;
        }

        if ((plantTags & PlantTags.Green) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Green";
            first = false;
        }

        if ((plantTags & PlantTags.Blue) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Blue";
            first = false;
        }

        if ((plantTags & PlantTags.Purple) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Purple";
            first = false;
        }

        if ((plantTags & PlantTags.White) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "White";
            first = false;
        }

        if ((plantTags & PlantTags.Black) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Black";
            first = false;
        }

        if ((plantTags & PlantTags.Flower) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Flower";
            first = false;
        }

        if ((plantTags & PlantTags.Leafy) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Green plant";
            first = false;
        }

        if ((plantTags & PlantTags.Big) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Big";
            first = false;
        }

        if ((plantTags & PlantTags.Small) != 0)
        {
            if (!first) tagStr += "\n";
            tagStr += "Small";
            first = false;
        }

        return tagStr;
    }
}
