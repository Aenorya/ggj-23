using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class SeedShop : MonoBehaviour
{
    public List<SeedChoice> choices = new List<SeedChoice>();
    public List<SeedData> possibleSeeds = new List<SeedData>();
    [SerializeField] private GameBalance _balance;
    private int lastUpdate = 0;

    private void Awake()
    {
        possibleSeeds = Resources.LoadAll<SeedData>("Seeds/").ToList();
    }

    private void OnEnable()
    {
        if (lastUpdate != TimeManager.day)
        {
            GenerateNewSeedChoice();
            lastUpdate = TimeManager.day;
        }

    }

    private void GenerateNewSeedChoice()
    {
        int rand = Random.Range(0, possibleSeeds.Count);
        choices[0].Init(possibleSeeds[rand]);
        
        rand = Random.Range(0, possibleSeeds.Count);
        choices[1].Init(possibleSeeds[rand]);
        
        rand = Random.Range(0, possibleSeeds.Count);
        choices[2].Init(possibleSeeds[rand]);
        
        rand = Random.Range(0, possibleSeeds.Count);
        choices[3].Init(possibleSeeds[rand]);
    }
}
