using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private Button play, quit;
    
    void Start()
    {
        play.onClick.AddListener(delegate
        {
            SceneManager.LoadScene(1);
        });
        quit.onClick.AddListener(delegate
        {
            Application.Quit();
        });
    }

    
    void Update()
    {
        
    }
}
